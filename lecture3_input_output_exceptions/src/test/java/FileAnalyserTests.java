import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.FileAnalyser;
import ru.edu.lecture3.FileAnalyserImpl;

import java.io.*;

public class FileAnalyserTests {

    private FileAnalyser fileAnalyser = null;

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getFileName()}
     * Возвращает имя файла
     * */
    @Test
    public void getFileNameTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals("countries.txt", fileAnalyser.getFileName());
        Assert.assertEquals(new File(filePath).getName(), fileAnalyser.getFileName());
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getRowsCount()}
     * Возвращает количество строк в файле
     * */
    @Test
    public void getRowsCountTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(3, fileAnalyser.getRowsCount());
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getRowsCount()}
     * Выбрасывает RuntimeException с сообщением "Файл не найден" если файл не найден
     * */
    @Test
    public void getRowsCountFileNotFoundExceptionTest() {
        String filePath = "./resources/countriesLALALA.txt";
        try {
            FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
            fileAnalyser.getRowsCount();
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
            Assert.assertEquals("Файл не найден", ex.getMessage());
        }
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getLettersCount()}
     * Возвращает количество букв в файле, буквы кириллические или латинского алфавита
     * */
    @Test
    public void getLettersCountTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(6, fileAnalyser.getLettersCount());
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getLettersCount()}
     * Возвращает Map количество вхождений символов [0-9A-Za-z]
     * */
    @Test
    public void getSymbolsStatisticsTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(11, fileAnalyser.getSymbolsStatistics().size());
        Assert.assertEquals(2, (int)fileAnalyser.getSymbolsStatistics().get('s'));
        Assert.assertEquals(2, (int)fileAnalyser.getSymbolsStatistics().get('4'));
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getTopNPopularSymbols(int)}
     * Возвращает List размерностью n самых популярных символов, при входящем аргументе равном n
     * */
    @Test
    public void getTopNPopularSymbolsReturnAmountSymbolsTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(2, fileAnalyser.getTopNPopularSymbols(2).size());
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#getTopNPopularSymbols(int)}
     * Возвращает List размерностью n самых популярных символов, при входящем аргументе равном n
     * */
    @Test
    public void getTopNPopularSymbolsTest() {
        String filePath = "./resources/countries.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(2, fileAnalyser.getTopNPopularSymbols(2).size());
        Assert.assertEquals(true, fileAnalyser.getTopNPopularSymbols(2).indexOf('s') >= 0);
        Assert.assertEquals(true, fileAnalyser.getTopNPopularSymbols(2).indexOf('4') >= 0);
    }

    /**
     * Проверка возвращаемого значения от {@link FileAnalyserImpl#saveSummary(String)}
     * Запись информации в файл, созданный файл равен эталонному
     * */
    @Test
    public void saveSummaryTest() {
        String filePath = "./resources/countries.txt";
        String resultFile = "./resources/summary.txt";
        String etalonfilePath = "./resources/etalonSummary.txt";
        FileAnalyserImpl fileAnalyser = new FileAnalyserImpl(filePath);
        fileAnalyser.saveSummary(resultFile);
        FileAnalyserImpl etalonFileAnalyser = new FileAnalyserImpl(etalonfilePath);
        FileAnalyserImpl resultFileAnalyser = new FileAnalyserImpl(resultFile);
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(resultFile));
            BufferedReader etalonBuffer = new BufferedReader(new FileReader(etalonfilePath));
            int lines = 1;
            if(etalonFileAnalyser.getRowsCount() == resultFileAnalyser.getRowsCount()) {
                String result = null;
                String etalonResult = null;
                while ((result = buffer.readLine()) != null && (etalonResult = etalonBuffer.readLine()) != null) {
                    if(result.equals(etalonResult)) {
                        lines++;
                    } else {
                        Assert.fail("Результат не равен эталонному, на строке " + lines);
                    }
                }
            } else {
                Assert.fail("Результат не равен эталонному, количество строк не совпадает");
                Assert.assertEquals(etalonFileAnalyser.getRowsCount(), resultFileAnalyser.getRowsCount());
            }
            buffer.close();
            etalonBuffer.close();
        } catch (Exception ex) {
            Assert.fail("Тест не состоялся!");
        }

    }
}