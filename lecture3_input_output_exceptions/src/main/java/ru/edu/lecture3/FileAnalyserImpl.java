package ru.edu.lecture3;

import java.io.*;
import java.util.*;

public class FileAnalyserImpl implements FileAnalyser {
    private String filePath;

    public FileAnalyserImpl(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String getFileName() {
        return new File(filePath).getName();
    }

    @Override
    public int getRowsCount() {
        int lines = 0;
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(filePath));
            while (buffer.readLine() != null) {
                lines++;
            }
            buffer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Файл не найден");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Нет доступа к файлу");
        }
        return lines;
    }

    @Override
    public int getLettersCount() {
        int letters = 0;
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(filePath));
            String bufferString;
            while ((bufferString = buffer.readLine()) != null) {
                letters += bufferString.replaceAll("[^A-Za-z]", "").length();
            }
            buffer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Файл не найден");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Нет доступа к файлу");
        }

        return letters;
    }

    @Override
    public Map<Character, Integer> getSymbolsStatistics() {
        Map<Character, Integer> map = new HashMap<>();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(filePath));
            String bufferString;
            while ((bufferString = buffer.readLine()) != null) {
                bufferString = bufferString.replaceAll("[^0-9A-Za-z]", "");
                if (bufferString.length() != 0) {
                    for (int i = 0; i < bufferString.length(); i++) {
                        Character symbol = bufferString.charAt(i);
                        if (map.containsKey(symbol)) {
                            int occurrence = map.get(symbol);
                            map.put(symbol, ++occurrence);
                        } else {
                            map.put(symbol, 1);
                        }
                    }
                }
            }
            buffer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Файл не найден");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Нет доступа к файлу");
        }

        return map;
    }

    @Override
    public List<Character> getTopNPopularSymbols(int n) {
        Map<Character, Integer> map = getSymbolsStatistics();
        List<Map.Entry<Character, Integer>> list = new ArrayList<>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> a, Map.Entry<Character, Integer> b) {
                return b.getValue() - a.getValue();
            }
        });

        List<Character> sortedList = new ArrayList<>();
        for (Map.Entry<Character, Integer> entry : list) {
            sortedList.add(entry.getKey());
        }

        return sortedList.subList(0, n);
    }

    @Override
    public void saveSummary(String filePath) {
        StringBuilder symbolsStatistics = new StringBuilder("symbolsStatistics: {");
        for (Map.Entry<Character, Integer> entry : getSymbolsStatistics().entrySet()) {
            symbolsStatistics.append(String.format("'%c': %d,", entry.getKey(), entry.getValue()));
        }
        symbolsStatistics.setCharAt(symbolsStatistics.length() - 1, '}');

        StringBuilder top3PopularSymbols = new StringBuilder("top3PopularSymbols: ");
        for(Character character : getTopNPopularSymbols(3)) {
            top3PopularSymbols.append(String.format("'%c', ", character));
        }
        top3PopularSymbols.delete(top3PopularSymbols.length() - 2, top3PopularSymbols.length());

        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write("fileName: " + new File(this.filePath).getName() + "\n");
            writer.write("rowsCount: " + getRowsCount() + "\n");
            writer.write("totalSymbols: " + getLettersCount() + "\n");
            writer.write(symbolsStatistics + "\n");
            writer.write(top3PopularSymbols + "\n");
            writer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Файл не найден");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Нет доступа к файлу");
        }
    }
}
