import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.Gender;
import ru.edu.lecture3.User;
import ru.edu.lecture3.UserStorage;
import ru.edu.lecture3.UserStorageImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;

public class UserStorageTests {

    private UserStorage userStorage = null;

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Добавляет User, если объект пользователя был корректный, пользователя нет в хранилище
     * */
    @Test
    public void putNewUserTest() {
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        UserStorageImpl storage = new UserStorageImpl();
        Assert.assertEquals(user, storage.put(user));
        Assert.assertEquals(1, storage.getAllUsers().size());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Обновляет User, если объект пользователя был корректный, и пользователь с таким login есть в хранилище
     * */
    @Test
    public void putNewRefreshUserTest() {
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        HashSet<User> users = new HashSet<>();
        users.add(user);
        UserStorageImpl storage = new UserStorageImpl(users);
        User changUser = new User("Ivan", "Гарри", "Поттер", Gender.MALE, LocalDate.of(1914, 7, 28));

        Assert.assertEquals(user, storage.put(changUser));
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(storage.getUserByLogin(user.getLogin()).getFirstName(), changUser.getFirstName());
        Assert.assertEquals(storage.getUserByLogin(user.getLogin()).getLastName(), changUser.getLastName());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Выбрасывает RuntimeException, если у объекта User пустой login
     * */
    @Test
    public void putEmptyLoginTest() {
        User user = new User("", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        UserStorageImpl storage = new UserStorageImpl();
        try {
            storage.put(user);
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Выбрасывает RuntimeException, если у объекта User login из пробелов
     * */
    @Test
    public void putSpaceLoginTest() {
        User user = new User(" ", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        UserStorageImpl storage = new UserStorageImpl();
        try {
            storage.put(user);
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Выбрасывает RuntimeException, если у объекта User gender null
     * */
    @Test
    public void putNullGenderTest() {
        User user = new User("Ivan", "Иван", "Иванов", null, LocalDate.of(1914, 7, 28));
        UserStorageImpl storage = new UserStorageImpl();
        try {
            storage.put(user);
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#put(User)}
     * Выбрасывает RuntimeException, если у объекта User birthDate null
     * */
    @Test
    public void putNullBirthDateTest() {
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, null);
        UserStorageImpl storage = new UserStorageImpl();
        try {
            storage.put(user);
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserByLogin(String)}
     * Возвращает User, если User с таким login найден
     * */
    @Test
    public void getUserByLoginTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        //HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(Vasya, storage.getUserByLogin(Vasya.getLogin()));
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserByLogin(String)}
     * Выбрасывает RuntimeException, если пользователь не найден
     * */
    @Test
    public void getUserByLoginUserNotFoundTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge("Marat");
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserByLogin(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный (пустой)
     * */
    @Test
    public void getUserByLoginLoginJunkTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserByLogin("");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
        try {
            storage.getUserByLogin(" ");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserByLogin(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный null
     * */
    @Test
    public void getUserByLoginLoginNullTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserByLogin(null);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserAge}
     * Если User с таким login найден, возвращает его возраст
     * */
    @Test
    public void getUserAgeTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        //HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(107, storage.getUserAge(Vasya.getLogin()));
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserAge(String)}
     * Выбрасывает RuntimeException, если пользователь не найден
     * */
    @Test
    public void getUserAgeUserNotFoundTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge("Marat");
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserAge(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный (пустой)
     * */
    @Test
    public void getUserAgeLoginJunkTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge("");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
        try {
            storage.getUserAge(" ");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getUserAge(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный null
     * */
    @Test
    public void getUserAgeLoginNullTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge(null);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#remove(String)}
     * Если User с таким login найден, удалить его из хранилища
     * */
    @Test
    public void removeTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        //HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(Ivan, storage.remove(Ivan.getLogin()));
        Assert.assertEquals(1, users.size());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#remove(String)}
     * Выбрасывает RuntimeException, если пользователь не найден
     * */
    @Test
    public void removeUserNotFoundTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge("Marat");
            Assert.fail("Ожидается RuntimeException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#remove(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный (пустой)
     * */
    @Test
    public void removeLoginJunkTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge("");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
        try {
            storage.getUserAge(" ");
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#remove(String)}
     * Выбрасывает IllegalArgumentException, если login пользователя не корректный null
     * */
    @Test
    public void removeLoginNullTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        try {
            storage.getUserAge(null);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (RuntimeException ex) {
        }
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getAllUsers()}
     * Возвращает всех хранимых пользователей (List<User>), пользователи хранятся в ArrayList
     * */
    @Test
    public void getAllUsersListTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        ArrayList<User> users = new ArrayList<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(users.size(), storage.getAllUsers().size());
        Assert.assertEquals(Vasya.getLogin(), storage.getUserByLogin(Vasya.getLogin()).getLogin());
        Assert.assertEquals(Ivan.getLogin(), storage.getUserByLogin(Ivan.getLogin()).getLogin());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getAllUsers()}
     * Возвращает всех хранимых пользователей (List<User>), пользователи хранятся в HashSet
     * */
    @Test
    public void getAllUsersSetTest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(users.size(), storage.getAllUsers().size());
        Assert.assertEquals(Ivan.getLogin(), storage.getUserByLogin(Ivan.getLogin()).getLogin());
        Assert.assertEquals(Vasya.getLogin(), storage.getUserByLogin(Vasya.getLogin()).getLogin());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getAllUsersByGender(Gender)}
     * Возвращает всех пользователей определенного пола (List<User>)
     * Возвращает пользователей с gender = FEMALE
     * */
    @Test
    public void getAllUsersByGenderFEMALETest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        User Masha = new User("Masha", "Мария", "Мариевна", Gender.FEMALE, LocalDate.of(1930, 11, 22));
        HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        users.add(Masha);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(1, storage.getAllUsersByGender(Gender.FEMALE).size());
    }

    /**
     * Проверка возвращаемого значения от {@link UserStorage#getAllUsersByGender(Gender)}
     * Возвращает всех пользователей определенного пола (List<User>)
     * Возвращает пользователей с gender = MALE
     * */
    @Test
    public void getAllUsersByGenderMALETest() {
        User Vasya = new User("Vasya", "Васильевич", "Василий", Gender.MALE, LocalDate.of(1914, 12, 22));
        User Ivan = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        User Masha = new User("Masha", "Мария", "Мариевна", Gender.FEMALE, LocalDate.of(1930, 11, 22));
        HashSet<User> users = new HashSet<>();
        users.add(Vasya);
        users.add(Ivan);
        users.add(Masha);
        UserStorageImpl storage = new UserStorageImpl(users);
        Assert.assertEquals(2, storage.getAllUsersByGender(Gender.MALE).size());
    }
}