import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.Gender;
import ru.edu.lecture3.User;
import java.time.LocalDate;

public class UserTests {

    /**
     * Проверка возвращаемого значения от {@link User#toString()}
     * */
    @Test
    public void toStringTest() {
        String expected = "Ivan: Иванов Иван (1914-07-28) - MALE";
        LocalDate birthDate = LocalDate.of(1914, 7, 28);
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, birthDate);
        Assert.assertEquals(expected, user.toString());
    }

    /**
     * Проверка возвращаемого значения от {@link User#getInformation()}
     * */
    /*@Test
    public void getInformationTest() {
        String expected = "IvanИвановИван1914-07-28MALE";
        LocalDate birthDate = LocalDate.of(1914, 7, 28);
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, birthDate);
        Assert.assertEquals(expected, user.getInformation());
    }*/

    /**
     * Проверка возвращаемого значения от {@link User#hashCode()}
     * */
    @Test
    public void hashCodeTest() {
        int expected = "Ivan".hashCode();
        LocalDate birthDate = LocalDate.of(1914, 7, 28);
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, birthDate);
        Assert.assertEquals(expected, user.hashCode());
    }

    /**
     * Проверка возвращаемого значения от {@link User#equals(Object)}
     * Возвращает ture (Объекты одинаковые), объекты имеют разные ссылки
     * */
    @Test
    public void equalsCloneTest() {
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        User cloneUser = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        Assert.assertEquals(true, user.equals(cloneUser));
    }

    /**
     * Проверка возвращаемого значения от {@link User#equals(Object)}
     * Возвращает ture (Объекты одинаковые), объекты имеют одну ссылку
     * */
    @Test
    public void equalsByMyselfTest() {
        User user = new User("Ivan", "Иван", "Иванов", Gender.MALE, LocalDate.of(1914, 7, 28));
        Assert.assertEquals(true, user.equals(user));
    }
}