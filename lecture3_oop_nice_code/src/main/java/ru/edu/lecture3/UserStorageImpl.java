package ru.edu.lecture3;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class UserStorageImpl implements UserStorage {
    private final Collection<User> users;

    public UserStorageImpl() {
        users = new HashSet<>();
    }

    public UserStorageImpl(Collection<User> users) {
        this.users = users;
    }

    @Override
    public User put(User user) {
        if (isUserCorrect(user)) {
            if (users.contains(user)) {
                remove(user.getLogin());
            }
            users.add(user);
            return user;
        }
        throw new RuntimeException("Не корректный пользователь");
    }

    @Override
    public User getUserByLogin(String login) {
        if (login == null || login.isEmpty()) throw new IllegalArgumentException("Не корректный аргумент");

        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            User curUser = iterator.next();
            if (curUser.hashCode() == login.hashCode()) {
                return curUser;
            }
        }
        throw new RuntimeException("Пользователь не найден");
    }

    @Override
    public User remove(String login) {
        User curUser = getUserByLogin(login);
        users.remove(curUser);
        return curUser;
    }

    @Override
    public int getUserAge(String login) {
        return Period.between(getUserByLogin(login).getBirthDate(), LocalDate.now()).getYears();
    }

    @Override
    public List<User> getAllUsers() {
        Object[] obj = users.toArray();
        List list = new ArrayList<>(Arrays.asList(obj));
        return list;
    }

    @Override
    public List<User> getAllUsersByGender(Gender gender) {
        List<User> sortedGenderList = new ArrayList<>();
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            User curUser = iterator.next();
            if (curUser.getGender() == gender) {
                sortedGenderList.add(curUser);
            }
        }
        return sortedGenderList;
    }

    /**
     * Returns a boolean value if the user has valid data
     *
     * @param user - input user
     * @return boolean value, true - if the user has valid data
     */
    private boolean isUserCorrect(User user) {
        if (user.getLogin().isEmpty() || user.getLastName().isEmpty() || user.getFirstName().isEmpty()) return false;
        else if (user.getGender() == null || user.getBirthDate() == null) return false;
        return true;
    }


}
