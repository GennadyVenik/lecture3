package ru.edu.lecture3;

import java.time.LocalDate;
import java.util.Collection;

/**
 * Contains user info.
 */
public class User {

    private String login;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;

    public User(String login, String firstName, String lastName, Gender gender, LocalDate birthDate) {
        setLogin(login);
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setBirthDate(birthDate);
    }

    @Override
    public int hashCode() {
        return getLogin().hashCode();
    }

    @Override
    public boolean equals(Object aUser) {
        User user = (User) aUser;
        return getLogin().equals(user.getLogin());
    }

    @Override
    public String toString() {
        return login + ": " + lastName + " " + firstName + " (" + birthDate + ") - " + gender;
    }

    public void setLogin(String login) {
        this.login = login.trim();
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName.trim();
    }
    public void setLastName(String lastName) {
        this.lastName = lastName.trim();
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getLogin() {
        return login;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }
    public Gender getGender() {
        return gender;
    }
    /*public String getInformation() {
        return login + lastName + firstName + birthDate + gender;
    }*/
}
